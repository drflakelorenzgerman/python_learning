

#==================================== VARIABLE


# fifty = 50
# ten = 10
#
# sum = ten + fifty
#
# div = fifty / ten
# print(div) #ba ashar
# div_ = fifty // ten
# print(div_) #desimal
#

#==================================== STRING



# str = "Ali" + "Shobeyri"
# print(str)
# str = str * 3
# print(str)
# print(str[1]) #l
# print(str[-1]) #i
# print(str[0:3]) #Ali
# print(str[3:]) # from 3 to end
# print(str[3:1000]) #from 3 to end
# print(str[:3]) # from start to 3 , Ali


#==================================== LIST


# list1 = [1,"str",[1,2]]
# print(list1)
# print(list1[1])
# print(list1[1:3]) # like string
# print(list1[2][0:2]) # [1,2]
#
# list1.append("Some") #add some in the end
# print(list1)
#
# list1.extend([1,2,3])
# print(list1) # concat a list into another list
#
# list1.insert(2,"SSS") # add item into list


#==================================== FUNCTION

# def outterFunc():
#     print("Hey Outter")
#     def innerFunc():
#         print("Hey Inner")
#     innerFunc()
#
# outterFunc()


# def func(arg1,arg2):
#     print(arg1)
#     print(arg2)
#     arg2(1)
#     return 30
#
#     # middleVar = arg1+arg2
#     # print(middleVar)
#
# def otherFunc():
#     print("SOMETHING")
#
# def otherFunc2(arg1):
#     print(arg1)
# # func(12,43)
# # func(12,otherFunc)
# print(func(12,otherFunc2))
#
#
# def anotherFuc(arg1,arg2):
#     print(arg1)
#     print(arg2)
#
# anotherFuc(arg2=1,arg1="S")


# def multiArgsFunc(*args,**kwargs): # kwargs -> key value args
#     for item in args:
#         print(item)
#     print(kwargs)
#
# multiArgsFunc(1,2,3,4,5,6,b = "sadas")
#
#
#
# def decoratePrintSome(func):
#     def wrapper():
#         print("Before")
#         func()
#         print("After")
#     return wrapper
#
#
# @decoratePrintSome
# def printSome():
#     print("Print Some")
#
# printSome()
#
# printSome2 = decoratePrintSome(printSome) # if you don't want use @
# printSome2()


# print(range(5))
# print(range(-3,5))
# print(range(-3,5,2)) # third parameter is increment step
#
# setww = set(['a','b','f'])
# print(setww)
#

#==================================== LOOP



# fruits = ['apple','banana','i dont know']
# for value in fruits:
#     print(value)
#
# for value in range(2,6):
#     print(value)

# number = input("Enter a num")
# for value in range(0,number):
#     print(value)

# a = "he"
# b = 12
# a = str(b)
# print(a)


# x = 1
# y = 2
#
# if(x == y):
#     print("Eq")
# elif(x < y):
#     print ("<")
# else:
#     print (">")


#==================================== FILE


# open("file.txt") #default is read mode
# file = open("file.txt","r") #default is read mode
# print(file.readline(4)) # 4 chars of that line
# print(file.readline())  # entire line
# print(file.readlines())  # the rest


# try:
#     file = open("filse.txt", "rb")  # default is read mode
#     print(file.readlines())  # the rest
# except:
#     print "WTF"
#
# try:
#     # file = open("filse.txt", "rb")  # default is read mode
#     # print(file.readlines())  # the rest
#     #
#     # file.close() # python has garbage  collector for this too
#
#     with open("file.txt","w") as file: # better soulotion
#         # print file.read()
#         file.write("New File")
#         file.writelines(["Hello","Yeap","Cool"])
# except Exception as e:
#     print e


# try:
#     # file = open("filse.txt", "rb")  # default is read mode
#     # print(file.readlines())  # the rest
#     #
#     # file.close() # python has garbage  collector for this too
#
#     with open("file.txt","r") as file: # better soulotion
#         data = file.read()
#         wordList = data.split("l") # default is space
#         print wordList
#
# except Exception as e:
#     print e

# print sum(len(word) for word in "hello")



#==================================== SQlite


import sqlite3
connection = sqlite3.connect("Database.db")
cursor = connection.cursor()
cursor.execute("""CREATE TABLE FOLAN
                (id integer primary key, name text, money real)
                """)
cursor.execute("""INSERT INTO FOLAN
                VALUES(1,"Ali",10.3)
                """)
cursor.execute("""INSERT INTO FOLAN(name , money)
                VALUES("Ali",10.3)
                """)